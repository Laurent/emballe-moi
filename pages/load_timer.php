<?php
	// Connexion à la BDD
	require_once '../includes/config.php';

  // On récupère le les questions visibles
  $req = $odb -> prepare('SELECT * FROM questions WHERE visible = 1');
  $req -> execute();
  $question = $req -> fetch();

  // On compte le nombre de résultats par rapport à la question et à l'utilisateur
  $countResultat = $odb->prepare('SELECT COUNT(*) FROM resultats WHERE idquestion=:idquestion AND iduser=:iduser');
  $countResultat->execute(array('idquestion' => $question['id'], 'iduser' => $_SESSION['idmembre']));
  $nbResultat = $countResultat->fetchColumn(0);

  // Si il existe aucun résultat
  if ($nbResultat == 0) {
    // On récupère le timestamp actuel
    $tempsActuel = time();
    $tempsRestant = $question['temps']-$tempsActuel+30;

    // Si le temps restant est supérieur à zéro
    if($tempsRestant>0){
      echo 'Il te reste ' . $tempsRestant . ' secondes pour répondre à la question.<br />';
    }

    else{
      echo 'Dépêche-toi !';
    }
  }
?>
